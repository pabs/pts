This is the code of the PTS web interface.

Principle :
-----------

The system gathers data in 'incoming/' from various sources (this is
done by bin/update_incoming.sh). Those data are merged in a set of XML
file in the 'base/' directory (this is done by bin/*_to_xml.py). Those
XML files are then transformed in HTML files (and other RDF formats,
put in the 'web/' directory) with xsltproc and the stylesheets inside
xsl/.

See the bin/do_all.sh script for more details (the one run by the
crontab).

You may as well check https://wiki.debian.org/qa.debian.org/pts for
some more documentation.

Dependencies :
--------------

- xsltproc
- mhonarc
- python
- python-lxml
- python-soappy (for SOAP access to the BTS)
- python-zsi (SOAP interface offered by the PTS)
- python-debian
- python-yaml
- raptor-utils (conversion between Turtle and RDF/XML)

Installation :
--------------

The "web" directory should be set as DocumentRoot of your VirtualHost.

Example of apache config (adapt it to your case)
<VirtualHost master>
	     ServerName packages.qa.debian.org
	     DocumentRoot /srv/packages.qa.debian.org/www/web
	     ErrorLog /var/log/apache2/packages.qa.debian.org-error.log
	     CustomLog /var/log/apache2/packages.qa.debian.org-access.log combined
	     
	     # All our html pages are UTF-8
	     AddType "text/html; charset=UTF-8" html
	     
	     ScriptAlias /cgi-bin /srv/packages.qa.debian.org/www/cgi-bin
	     ErrorDocument 404 /cgi-bin/error404.cgi
	     
	     RewriteEngine on

	     # Distinguish between RDF or HTML serving
	     RewriteCond %{HTTP_ACCEPT} !(application/rdf\+xml)
	     # If the Accept header doesn't request RDF+XML, then skip the following 2 lines
	     RewriteRule .* - [S=2]
	     RewriteRule ^/\s*lib([^/\s])([^/\s]+)\s*$ /lib$1/lib$1$2.rdf [L,R]
	     RewriteRule ^/\s*([^/\s])([^/\s]+)\s*$ /$1/$1$2.rdf [L,R]
	     
	     RewriteCond %{HTTP_ACCEPT} !(text/turtle)
	     # If the Accept header doesn't request Turtle, then skip the following 2 lines
	     RewriteRule .* - [S=2]
	     RewriteRule ^/\s*lib([^/\s])([^/\s]+)\s*$ /lib$1/lib$1$2.ttl [L,R]
	     RewriteRule ^/\s*([^/\s])([^/\s]+)\s*$ /$1/$1$2.ttl [L,R]
	     
	     RewriteRule ^/$ /common/index.html [L,R]
	     RewriteRule ^/favicon\.ico$ https://www.debian.org/favicon.ico [L,R]
	     RewriteRule ^/\s*lib([^/\s])([^/\s]+)\s*$ /lib$1/lib$1$2.html [L,R]
	     RewriteRule ^/\s*([^/\s])([^/\s]+)\s*$ /$1/$1$2.html [L,R]
	     
	     RewriteCond %{QUERY_STRING} ^src=\s*lib([^\s])([^\s]+)\s*$
	     RewriteRule ^/common/index.html$ /lib%1/lib%1%2.html? [L,R,NE]
	     RewriteCond %{QUERY_STRING} ^src=\s*([^\s])([^\s]+)\s*$
	     RewriteRule ^/common/index.html$ /%1/%1%2.html? [L,R,NE]
	     
	     RewriteCond %{QUERY_STRING} ^srcrdf=\s*lib([^\s])([^\s]+)\s*$
	     RewriteRule ^/common/RDF.html$ /lib%1/lib%1%2.ttl? [L,R,NE]
	     RewriteCond %{QUERY_STRING} ^srcrdf=\s*([^\s])([^\s]+)\s*$
	     RewriteRule ^/common/RDF.html$ /%1/%1%2.ttl? [L,R,NE]

</VirtualHost>

Create these directories if they're not yet present :
$ mkdir incoming
$ mkdir base
