<?xml version="1.0" encoding="utf-8" ?>

<!--
# Copyright © 2002-2011 Raphaël Hertzog and others
# Copyright © 2007-2009 Stefano Zacchiroli
# This file is distributed under the terms of the General Public License
# version 2 or (at your option) any later version.
-->

<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0">

<!-- Common parameters and variables definitions to pts.xsl and admssw.xsl -->

<xsl:param name="gitrev"/>
<xsl:param name="package"/>
<xsl:param name="dir"/>
<xsl:param name="date"/>
<xsl:param name="hasoldoldstable" select="''"/>
<xsl:param name="hasoldstable" select="''"/>
<xsl:param name="hasstable" select="''"/>
<xsl:param name="hastesting" select="''"/>
<xsl:param name="hasunstable" select="''"/>
<xsl:param name="hasexperimental" select="''"/>
<xsl:param name="hasmentors" select="''"/>
<xsl:param name="hasother" select="''"/>
<xsl:param name="hasoldoldstable-proposed-updates" select="''"/>
<xsl:param name="hasoldstable-proposed-updates" select="''"/>
<xsl:param name="hasstable-proposed-updates" select="''"/>
<xsl:param name="hastesting-proposed-updates" select="''"/>
<xsl:param name="hassecurity-oldoldstable" select="''"/>
<xsl:param name="hassecurity-oldstable" select="''"/>
<xsl:param name="hassecurity-stable" select="''"/>
<xsl:param name="hassecurity-testing" select="''"/>
<xsl:param name="hasoldoldstable-updates" select="''"/>
<xsl:param name="hasoldstable-updates" select="''"/>
<xsl:param name="hasstable-updates" select="''"/>
<xsl:param name="hasoldoldstable-backports" select="''"/>
<xsl:param name="hasoldoldstable-backports-sloppy" select="''"/>
<xsl:param name="hasoldstable-backports" select="''"/>
<xsl:param name="hasoldstable-backports-sloppy" select="''"/>
<xsl:param name="hasstable-backports" select="''"/>
<xsl:param name="hasnews" select="''"/>
<xsl:param name="hasexcuse" select="''"/>

<xsl:variable name="other"
  select="document(concat('../base/', $dir, '/other.xml'))/other"/>

<xsl:variable name="mostrecentsuite">
  <!-- name of the most recent suite in which the package is available
  -->
  <xsl:choose>
    <xsl:when test="$hasexperimental">
      <xsl:text>experimental</xsl:text>
    </xsl:when>
    <xsl:when test="$hasunstable">
      <xsl:text>unstable</xsl:text>
    </xsl:when>
    <xsl:when test="$hastesting">
      <xsl:text>testing</xsl:text>
    </xsl:when>
    <xsl:when test="$hasstable">
      <xsl:text>stable</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text />
    </xsl:otherwise>
  </xsl:choose>
</xsl:variable>
<xsl:variable name="pooldir">
  <xsl:if test="$mostrecentsuite != ''">
    <xsl:value-of
       select="document(concat('../base/', $dir, '/',
	       $mostrecentsuite, '.xml'))/source/directory" />
  </xsl:if>
</xsl:variable>

<xsl:variable name="mirror">http://deb.debian.org/debian</xsl:variable>

</xsl:stylesheet>
