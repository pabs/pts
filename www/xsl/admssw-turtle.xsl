<?xml version="1.0" encoding="utf-8" ?>

<!--
# Copyright © 2002-2011 Raphaël Hertzog and others
# Copyright © 2007-2009 Stefano Zacchiroli
# Copyright © 2012 Olivier Berger & Institut Mines Telecom
# This file is distributed under the terms of the General Public License
# version 2 or (at your option) any later version.

This XSLT stylesheet is meant to output RDF/Turtle meta-data about source packages in unstable. See #685605 for more details.

The duplicate filtering algorithm is borrowed from Doug Tidwell's examples in XSLT book from O'Reilly
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">
    
  <xsl:output method="text"/>

<!-- Include common parameters and variables definitions to pts.xsl and admssw.xsl -->
<xsl:include href="common-params-vars.xsl" />

<xsl:variable name="released-URI">http://packages.qa.debian.org/#released</xsl:variable>

<!-- The following template copied from https://skew.org/xml/stylesheets/url-encode/url-encode.xsl

       ISO-8859-1 based URL-encoding demo
       Written by Mike J. Brown, mike@skew.org.
       Updated 2002-05-20.

       No license; use freely, but credit me if reproducing in print.

       Also see https://skew.org/xml/misc/URI-i18n/ for a discussion of
       non-ASCII characters in URIs.
-->
<!-- The string to URL-encode.
     Note: By "iso-string" we mean a Unicode string where all
     the characters happen to fall in the ASCII and ISO-8859-1
     ranges (32-126 and 160-255) -->
<!-- Characters we'll support.
     We could add control chars 0-31 and 127-159, but we won't. -->
<xsl:variable name="ascii"> !"#$%&amp;'()*+,-./0123456789:;&lt;=&gt;?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~</xsl:variable>
<xsl:variable name="latin1">&#160;&#161;&#162;&#163;&#164;&#165;&#166;&#167;&#168;&#169;&#170;&#171;&#172;&#173;&#174;&#175;&#176;&#177;&#178;&#179;&#180;&#181;&#182;&#183;&#184;&#185;&#186;&#187;&#188;&#189;&#190;&#191;&#192;&#193;&#194;&#195;&#196;&#197;&#198;&#199;&#200;&#201;&#202;&#203;&#204;&#205;&#206;&#207;&#208;&#209;&#210;&#211;&#212;&#213;&#214;&#215;&#216;&#217;&#218;&#219;&#220;&#221;&#222;&#223;&#224;&#225;&#226;&#227;&#228;&#229;&#230;&#231;&#232;&#233;&#234;&#235;&#236;&#237;&#238;&#239;&#240;&#241;&#242;&#243;&#244;&#245;&#246;&#247;&#248;&#249;&#250;&#251;&#252;&#253;&#254;&#255;</xsl:variable>
<!-- Characters that usually don't need to be escaped -->
<xsl:variable name="safe">!'()*-.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz~</xsl:variable>
<xsl:variable name="hex" >0123456789ABCDEF</xsl:variable>
<xsl:template name="url-encode">
  <xsl:param name="str"/>   
  <xsl:if test="$str">
    <xsl:variable name="first-char" select="substring($str,1,1)"/>
    <xsl:choose>
      <xsl:when test="contains($safe,$first-char)">
	<xsl:value-of select="$first-char"/>
      </xsl:when>
      <xsl:otherwise>
	<xsl:variable name="codepoint">
	  <xsl:choose>
	    <xsl:when test="contains($ascii,$first-char)">
	      <xsl:value-of select="string-length(substring-before($ascii,$first-char)) + 32"/>
	    </xsl:when>
	    <xsl:when test="contains($latin1,$first-char)">
	      <xsl:value-of select="string-length(substring-before($latin1,$first-char)) + 160"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:message terminate="no">Warning: string contains a character that is out of range! Substituting "?".</xsl:message>
	      <xsl:text>63</xsl:text>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:variable>
        <xsl:variable name="hex-digit1" select="substring($hex,floor($codepoint div 16) + 1,1)"/>
        <xsl:variable name="hex-digit2" select="substring($hex,$codepoint mod 16 + 1,1)"/>
        <xsl:value-of select="concat('%',$hex-digit1,$hex-digit2)"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="string-length($str) &gt; 1">
      <xsl:call-template name="url-encode">
	<xsl:with-param name="str" select="substring($str,2)"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:if>
</xsl:template>
<!-- END OF url-encode -->

<!-- Needed for lowercase implemented as : select="translate($text, $uppercase, $smallcase)" -->
<xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />



<!-- Loads the suite's version XML file to find that suite's version -->
<xsl:template name="suite-version">
  <xsl:param name="suite" select="''" />
  <xsl:variable name="version">
      <xsl:value-of select="document(concat('../base/', $dir, '/', $suite,
			    '.xml'))/source/version"/>
  </xsl:variable>
  <xsl:copy-of select="$version" />
</xsl:template>


<!-- Lists all versions in all suites separated by spaces -->
<xsl:variable name="list-of-versions">

      <xsl:if test="$hasoldstable">  
	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">oldstable</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hassecurity-oldstable">
	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">security-oldstable</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hasoldstable-updates">
      	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">oldstable-updates</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hasoldstable-proposed-updates">  
	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">oldstable-proposed-updates</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hasoldstable-backports">  
	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">oldstable-backports</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hasoldstable-backports-sloppy">  
	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">oldstable-backports-sloppy</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hasstable">  
	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">stable</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hassecurity-stable">
      	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">security-stable</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hasstable-updates">
      	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">stable-updates</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hasstable-proposed-updates">  
	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">stable-proposed-updates</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hasstable-backports">
      	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">stable-backports</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hastesting">
	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">testing</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hassecurity-testing">
	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">security-testing</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hastesting-proposed-updates">
	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">testing-proposed-updates</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hasunstable">
	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">unstable</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hasexperimental">
	  <xsl:call-template name="suite-version">
	    <xsl:with-param name="suite">experimental</xsl:with-param>
	  </xsl:call-template><xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="$hasother and $other/@new_version">
	  <xsl:value-of select="$other/@new_version" />
      </xsl:if>

</xsl:variable>

<!-- Can remove duplicates in a string of space separated values -->
<xsl:template name="remove-duplicates">
  <xsl:param name="list-of-versions"/>
  <xsl:param name="last-version" select="''"/>
  <xsl:variable name="next-version">
    <xsl:value-of select="substring-before($list-of-versions, ' ')"/>
  </xsl:variable>
  <xsl:variable name="remaining-versions">
    <xsl:value-of select="substring-after($list-of-versions, ' ')"/>
  </xsl:variable>
  <xsl:choose>
    <xsl:when test="not(string-length(normalize-space($list-of-versions)))">
        <!-- If the list of states is empty, do nothing -->
    </xsl:when>
    <xsl:when test="not($last-version=$next-version)">
      <xsl:value-of select="$next-version"/>
      <xsl:text> </xsl:text>
      <xsl:call-template name="remove-duplicates">
        <xsl:with-param name="list-of-versions" select="$remaining-versions"/>
        <xsl:with-param name="last-version" select="$next-version"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="$last-version=$next-version">
      <xsl:call-template name="remove-duplicates">
        <xsl:with-param name="list-of-versions" select="$remaining-versions"/>
        <xsl:with-param name="last-version" select="$next-version"/>
      </xsl:call-template>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<!-- Construct a list of unique versions -->
<xsl:variable name="list-of-unique-versions">
  <xsl:call-template name="remove-duplicates">
    <xsl:with-param name="list-of-versions" select="$list-of-versions"/>
    <xsl:with-param name="last-version" select="''"/>
  </xsl:call-template>
</xsl:variable>

<!-- may need to use mostrecentsuite instead ? but consider we apply to unstable at the moment -->
<xsl:variable name="latest-version">
  <xsl:value-of select="//version" />
</xsl:variable>


<!-- outputs an ADMS.SW Software Package -->
<xsl:template name="software-package">
  <xsl:param name="uri" />
  <xsl:param name="mode" />
  <xsl:param name="filename" />
  <xsl:param name="version" select="''" />
  <xsl:param name="filesize" />
  <xsl:param name="md5sum" />

  <!-- either a link to the resource -->
  <xsl:if test="$mode='link'">
    <xsl:text>
    admssw:package &lt;</xsl:text><xsl:value-of select="$uri" /><xsl:text>#</xsl:text><xsl:value-of select="$filename" /><xsl:text>&gt; ;</xsl:text>
  </xsl:if>

  <!-- or the full resource -->
  <xsl:if test="$mode='full'">

    <xsl:text>
&lt;</xsl:text><xsl:value-of select="$uri" /><xsl:text>#</xsl:text><xsl:value-of select="$filename" /><xsl:text>&gt;</xsl:text>
    <xsl:text>
    rdfs:label "</xsl:text><xsl:value-of select="$filename" /><xsl:text>" ;</xsl:text>
    <xsl:if test="not(substring-before(concat($filename, '.orig'), '.orig')=$filename)">
      <xsl:text>
    dcterms:description "Upstream source archive for </xsl:text><xsl:value-of select="$package" /><xsl:text> version </xsl:text><xsl:value-of select="$version" /><xsl:text> (potentially re-archived by Debian)" ;</xsl:text>
    </xsl:if>
    <xsl:if test="not(substring-before(concat($filename, '.dsc'), '.dsc')=$filename)">
      <xsl:text>
    dcterms:description "Debian source package descriptor file for </xsl:text><xsl:value-of select="$package" /><xsl:text> version </xsl:text><xsl:value-of select="$version" /><xsl:text>" ;</xsl:text>
    </xsl:if>
    <xsl:if test="substring-before(concat($filename, '.orig'), '.orig')=$filename">
      <xsl:if test="substring-before(concat($filename, '.dsc'), '.dsc')=$filename">
	<xsl:text>
    dcterms:description "Debian source package files archive for </xsl:text><xsl:value-of select="$package" /><xsl:text> version </xsl:text><xsl:value-of select="$version" /><xsl:text>" ;</xsl:text>
      </xsl:if>
    </xsl:if>

    <xsl:text>
    schema:downloadUrl &lt;</xsl:text><xsl:value-of select="$mirror"/><xsl:text>/</xsl:text><xsl:value-of select="$pooldir"/><xsl:text>/</xsl:text><xsl:value-of select="$filename" /><xsl:text>&gt; ;</xsl:text><xsl:text>
    schema:fileSize "</xsl:text><xsl:value-of select="$filesize" /><xsl:text>" ;</xsl:text><xsl:text>
    spdx:checksum [</xsl:text><xsl:text>
      <!-- The SPDX doesn't seem to support md5sum, so fake one -->
      spdx:algorithm &lt;http://packages.qa.debian.org/#checksumAlgorithm_md5sum&gt; ;</xsl:text><xsl:text>
      spdx:checksumValue "</xsl:text><xsl:value-of select="$md5sum" /><xsl:text>"</xsl:text><xsl:text>
    ] ;</xsl:text><xsl:text>
    admssw:status &lt;</xsl:text><xsl:value-of select="$released-URI" /><xsl:text>&gt; ;</xsl:text><xsl:text>
    a admssw:SoftwarePackage .</xsl:text><xsl:text>

</xsl:text>
  </xsl:if>

</xsl:template>


<!-- create a link to a Software Package for a file which is neither .orig nor .dsc -->
<xsl:template name="link-package-if-debian-sources-file">
  <xsl:param name="filename" />

  <xsl:if test="substring-before(concat($filename, '.orig'), '.orig')=$filename">
    <xsl:if test="substring-before(concat($filename, '.dsc'), '.dsc')=$filename">
      <xsl:call-template name="software-package">
	<xsl:with-param name="uri" select="concat('http://packages.qa.debian.org/', $package)"/>
	<xsl:with-param name="mode" select="'link'"/>
	<xsl:with-param name="filename" select="$filename"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:if>

</xsl:template>


<!-- create a link to a Software Package for a file which is a .orig archive -->
<xsl:template name="link-to-package-if-upstream-sources-file">
  <xsl:param name="filename" />

  <xsl:if test="not(substring-before(concat($filename, '.orig'), '.orig')=$filename)">
    <xsl:call-template name="software-package">
      <xsl:with-param name="uri" select="concat('http://packages.qa.debian.org/', $package)"/>
      <xsl:with-param name="mode" select="'link'"/>
      <xsl:with-param name="filename" select="$filename"/>
    </xsl:call-template>
  </xsl:if>

</xsl:template>

<!-- outputs a Software Release for an includedAsset, i.e. a file part of the source packaging -->
<xsl:template name="included-asset">
  <xsl:param name="uri" />
  <xsl:param name="mode" />
  <xsl:param name="filename" />
  <xsl:param name="version" select="''" />

  <!-- either a link to a resource -->
  <xsl:if test="$mode='link'">
    <!-- outputs a <admssw:includedAsset rdf:resource="..." /> -->
    <xsl:text>
    admssw:includedAsset &lt;</xsl:text><xsl:value-of select="$uri" /><xsl:text>#</xsl:text>

	<xsl:choose>
	  <xsl:when test="$filename='_debian'">
	    <xsl:text>debiansrc_</xsl:text>
	    <xsl:value-of select="$version" />
	  </xsl:when>
	  <xsl:otherwise>
            <xsl:text>upstreamsrc_</xsl:text>
	    <xsl:value-of select="$version" />
	  </xsl:otherwise>
	</xsl:choose>

	<xsl:text>&gt; ;</xsl:text>

  </xsl:if>

  <!-- or the full resource itself -->
  <xsl:if test="$mode='full'">
    
    <xsl:choose>
      <!-- if the file is a the Debian packaging source part -->
      <xsl:when test="$filename='_debian'">

	<xsl:text>
&lt;</xsl:text><xsl:value-of select="$uri" /><xsl:text>#debiansrc_</xsl:text><xsl:value-of select="$version" /><xsl:text>&gt;</xsl:text><xsl:text>
    dcterms:description "Debian packaging sources for </xsl:text><xsl:value-of select="$package" /><xsl:text> version </xsl:text><xsl:value-of select="$version" /><xsl:text>" ;</xsl:text><xsl:text>
    rdfs:label "Package sources </xsl:text><xsl:value-of select="$package" /><xsl:text>_</xsl:text><xsl:value-of select="$version" /><xsl:text>" ;</xsl:text><xsl:text>
    doap:revision "</xsl:text><xsl:value-of select="$version" /><xsl:text>" ;</xsl:text><xsl:text>
    dcterms:publisher &lt;http://debian.org/&gt; ;</xsl:text><xsl:text>
    admssw:status &lt;</xsl:text><xsl:value-of select="$released-URI" /><xsl:text>&gt; ;</xsl:text>

	  <!-- add a link to the Software Package resource for the Debian packaging sources file -->
	  <xsl:for-each select="//files/item">
            <xsl:call-template name="link-package-if-debian-sources-file">
	      <xsl:with-param name="filename" select="./filename" />
	    </xsl:call-template>
	  </xsl:for-each>
	  <xsl:text>
    a admssw:SoftwareRelease .

</xsl:text>

      </xsl:when>

      <!-- or if the file is upstream part -->
      <xsl:otherwise>
	<xsl:text>
&lt;</xsl:text><xsl:value-of select="$uri" /><xsl:text>#upstreamsrc_</xsl:text><xsl:value-of select="$version" /><xsl:text>&gt;</xsl:text><xsl:text>
    dcterms:description "Upstream source release for </xsl:text><xsl:value-of select="$package" /><xsl:text> version </xsl:text><xsl:value-of select="$version" /><xsl:text>" ;</xsl:text><xsl:text>
    rdfs:label "Upstream </xsl:text><xsl:value-of select="$package" /><xsl:text> release </xsl:text><xsl:value-of select="$version" /><xsl:text>" ;</xsl:text><xsl:text>
    doap:revision "</xsl:text><xsl:value-of select="$version" /><xsl:text>" ;</xsl:text>
    <!-- Even though the upstream sources are in principle published by upstream project, the archives are often changed by debian --><xsl:text>
    dcterms:publisher &lt;http://debian.org/&gt; ;</xsl:text>
    <!-- As debian may be using beta releases of upstream projects, make up a particular status --><xsl:text>
    admssw:status &lt;http://packages.qa.debian.org/#snapshot_or_release&gt; ;</xsl:text>


	  <xsl:for-each select="//files/item">
            <xsl:call-template name="link-to-package-if-upstream-sources-file">
	      <xsl:with-param name="filename" select="./filename" />
	    </xsl:call-template>
	  </xsl:for-each>

	  <xsl:text>
    admssw:project &lt;</xsl:text><xsl:value-of select="$uri" /><xsl:text>#upstream&gt; ;</xsl:text><xsl:text>
    a admssw:SoftwareRelease .

</xsl:text><xsl:text>
&lt;</xsl:text><xsl:value-of select="$uri" /><xsl:text>#upstream&gt;</xsl:text><xsl:text>
    doap:description "The </xsl:text><xsl:value-of select="$package" /><xsl:text> upstream project" ;</xsl:text><xsl:text>
    doap:name "</xsl:text><xsl:value-of select="$package" />" ;<xsl:text></xsl:text>

	  <xsl:if test="//homepage != ''">
	    <xsl:text>
    doap:homepage &lt;</xsl:text><xsl:value-of select="//homepage" /><xsl:text>&gt; ;</xsl:text>

	  </xsl:if>

	  <xsl:text>
    a admssw:SoftwareProject .

</xsl:text>

      </xsl:otherwise>
    </xsl:choose>

  </xsl:if>

</xsl:template>

<xsl:template name="release-URI">
  <xsl:param name="version" select="''" />
  <xsl:text>http://packages.qa.debian.org/</xsl:text><xsl:value-of select="$package" /><xsl:text>#</xsl:text><xsl:value-of select="$package" /><xsl:text>_</xsl:text><xsl:value-of select="$version" />
</xsl:template>

<xsl:template name="project-URI">
  <xsl:text>http://packages.qa.debian.org/</xsl:text><xsl:value-of select="$package" /><xsl:text>#project</xsl:text>
</xsl:template>

<!-- Outputs an admssw:SoftwareRelease resource for a version -->
<xsl:template name="software-release">
  <xsl:param name="version" select="''" />
  <!-- First the source package version itself -->
<xsl:text>
&lt;</xsl:text><xsl:call-template name="release-URI"><xsl:with-param name="version" select="$version"/></xsl:call-template><xsl:text>&gt;</xsl:text><xsl:text>
    dcterms:description "Debian </xsl:text><xsl:value-of select="$package" /><xsl:text> source package version </xsl:text><xsl:copy-of select="$version" /><xsl:text>" ;</xsl:text><xsl:text>
    rdfs:label "</xsl:text><xsl:value-of select="$package" /><xsl:text> </xsl:text><xsl:value-of select="$version" /><xsl:text>" ;</xsl:text><xsl:text>
    doap:revision "</xsl:text><xsl:value-of select="$version" /><xsl:text>" ;
    admssw:project &lt;</xsl:text><xsl:call-template name="project-URI" /><xsl:text>&gt; ;</xsl:text><xsl:text>
    dcterms:publisher &lt;http://debian.org/&gt; ;</xsl:text><xsl:text>
    admssw:status &lt;</xsl:text><xsl:value-of select="$released-URI" /><xsl:text>&gt; ;</xsl:text>

    <!-- Only display links to includedAssets and Software Packages for the unstable version -->
    <xsl:if test="$version=$latest-version">  

      <xsl:choose>

	<!-- Native package -->
	<xsl:when test="substring-before(concat($version, '-'), '-')=$version">

	  <!-- point to all files as packages -->

	  <xsl:for-each select="//files/item">
	    <xsl:call-template name="software-package">
	      <xsl:with-param name="uri" select="concat('http://packages.qa.debian.org/', $package)"/>
	      <xsl:with-param name="mode" select="'link'"/>
	      <xsl:with-param name="filename" select="filename"/>
	    </xsl:call-template>
	  </xsl:for-each>

	</xsl:when>
	
	<!-- Non-native package : add links to includedAssets -->
	<xsl:otherwise>

	  <!-- link to the .dsc file as a Software Package -->
	  <xsl:call-template name="software-package">
	    <xsl:with-param name="uri" select="concat('http://packages.qa.debian.org/', $package)"/>
	    <xsl:with-param name="mode" select="'link'"/>
	    <xsl:with-param name="filename" select="//files/item[1]/filename"/>
	  </xsl:call-template>

	  <!-- link to the upstream Software Release resource -->
	  <xsl:call-template name="included-asset">
	    <xsl:with-param name="uri" select="concat('http://packages.qa.debian.org/', $package)"/>
	    <xsl:with-param name="mode" select="'link'"/>
	    <xsl:with-param name="filename" select="'_orig'"/>
	    <xsl:with-param name="version" select="substring-before(concat($version, '-'), '-')"/>
	  </xsl:call-template>
	  
	  <!-- link to the Debian parts (scripts, config files and patches) -->
	  <xsl:call-template name="included-asset">
	    <xsl:with-param name="uri" select="concat('http://packages.qa.debian.org/', $package)"/>
	    <xsl:with-param name="mode" select="'link'"/>
	    <xsl:with-param name="filename" select="'_debian'"/>
	    <xsl:with-param name="version" select="$version"/>
	  </xsl:call-template>

	</xsl:otherwise>

      </xsl:choose>

      <xsl:if test="$other/@ubuntu='yes'">
	<xsl:call-template name="related-ubuntu-release">
	  <xsl:with-param name="uri" select="$other/ubuntu/@url" />
	  <xsl:with-param name="version" select="$other/ubuntu/@version" />
	</xsl:call-template>
      </xsl:if>

    </xsl:if>


    <xsl:text>
    a admssw:SoftwareRelease .

</xsl:text>

  <!-- Then the additional resources linked from above -->
  <xsl:if test="$version=$latest-version">  

    <xsl:choose>

      <!-- Native package -->
      <xsl:when test="substring-before(concat($version, '-'), '-')=$version">
	<!-- do nothing -->
      </xsl:when>

      <!-- Non-native package : add resources for the includedAssets -->
      <xsl:otherwise>

	<!-- The upstream Software Release resource -->
	<xsl:call-template name="included-asset">
	  <xsl:with-param name="uri" select="concat('http://packages.qa.debian.org/', $package)"/>
	  <xsl:with-param name="mode" select="'full'"/>
	  <xsl:with-param name="filename" select="'_orig'"/>
	  <xsl:with-param name="version" select="substring-before(concat($version, '-'), '-')"/>
	</xsl:call-template>

	<!-- The Debian parts (scripts, config files and patches) resource -->
	<xsl:call-template name="included-asset">
	  <xsl:with-param name="uri" select="concat('http://packages.qa.debian.org/', $package)"/>
	  <xsl:with-param name="mode" select="'full'"/>
	  <xsl:with-param name="filename" select="'_debian'"/>
	  <xsl:with-param name="version" select="$version"/>
	</xsl:call-template>
      </xsl:otherwise>

    </xsl:choose>

    <!-- For all the files referenced in the source package, output a Software Package resource -->
    <xsl:for-each select="//files/item">
      <xsl:call-template name="software-package">
	<xsl:with-param name="uri" select="concat('http://packages.qa.debian.org/', $package)"/>
	<xsl:with-param name="mode" select="'full'"/>
	<xsl:with-param name="filename" select="filename"/>
	<xsl:with-param name="version" select="$version"/>
	<xsl:with-param name="filesize" select="size"/>
	<xsl:with-param name="md5sum" select="md5sum"/>
      </xsl:call-template>
    </xsl:for-each>

  </xsl:if>


</xsl:template>


<!-- Outputs a link to an admssw:SoftwareRelease resource for a version, to be used to link from an admssw:SoftwareProject -->
<xsl:template name="link-software-release">
  <xsl:param name="version" select="''" /><xsl:text>
    doap:release &lt;</xsl:text><xsl:call-template name="release-URI"><xsl:with-param name="version" select="$version"/></xsl:call-template><xsl:text>&gt; ;</xsl:text>
</xsl:template>

<!-- Recursively displays SoftwareRelease resources or links for a list of versions -->
<xsl:template name="iterate-software-releases">
  <xsl:param name="list-of-unique-versions"/>
  <xsl:param name="mode" select="''"/>
  <xsl:variable name="next-version">
    <xsl:value-of select="substring-before($list-of-unique-versions, ' ')"/>
  </xsl:variable>
  <xsl:variable name="remaining-versions">
    <xsl:value-of select="substring-after($list-of-unique-versions, ' ')"/>
  </xsl:variable>

  <!-- Outputs the full resource -->
  <xsl:if test="$mode='full'">
    <xsl:call-template name="software-release">
      <xsl:with-param name="version" 
		      select="$next-version"/>
    </xsl:call-template>
  </xsl:if>
  <!-- Or outputs a link to the resource -->
  <xsl:if test="$mode='link'">
    <xsl:call-template name="link-software-release">
      <xsl:with-param name="version" 
		      select="$next-version"/>
    </xsl:call-template>
  </xsl:if>

  <xsl:if test="normalize-space($remaining-versions)">
    <xsl:call-template name="iterate-software-releases">
      <xsl:with-param name="list-of-unique-versions" 
        select="$remaining-versions"/>
      <xsl:with-param name="mode" 
		      select="$mode"/>
    </xsl:call-template>
  </xsl:if>
</xsl:template>


<!-- Convert + in %2b for URL escaping. Should actually first also do
other-to-%xx, especially % to %25... Fortunately, that's rare -->
<xsl:template name="escape-name">
  <xsl:param name="text"/>
  <xsl:if test="contains($text,'+')">
    <xsl:value-of select="substring-before($text,'+')"/>
    <xsl:text>%2b</xsl:text>
    <xsl:call-template name="escape-name">
    <xsl:with-param name="text"><xsl:value-of select="substring-after($text,'+')"/></xsl:with-param>
    </xsl:call-template>
  </xsl:if>
  <xsl:if test="not(contains($text,'+'))">
    <xsl:value-of select="$text"/>
  </xsl:if>
</xsl:template>

<!-- Generates URis for FOAF documents on http://webid.debian.net/ - See https://wiki.debian.org/WebIDDebianNet -->
<xsl:template name="email-to-webid-uri">
  <xsl:param name="text"/>
  <xsl:text>http://webid.debian.net/maintainers/</xsl:text>
  <xsl:choose>
    <!-- When the maintainer doesn't use a debian.org email, url-encode it -->
    <xsl:when test="substring-before(concat($text, '@debian.org'), '@debian.org')=$text">
      <xsl:call-template name="url-encode">
	<xsl:with-param name="str" select="translate($text, $uppercase, $smallcase)"/>
      </xsl:call-template>
    </xsl:when>
    <!-- otherwise, use the debian login -->
    <xsl:otherwise>
      <xsl:value-of select="substring-before($text, '@debian.org')"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="online-account">
  <xsl:param name="escaped-email" />
  <xsl:param name="escaped-name" />
  <xsl:param name="webid-uri" />
<xsl:text>[</xsl:text><xsl:text>
      a foaf:Agent ;</xsl:text><xsl:text>
      foaf:name "</xsl:text><xsl:value-of select="$escaped-name"/><xsl:text>" ;</xsl:text><xsl:text>
      foaf:account [</xsl:text><xsl:text>
         a foaf:OnlineAccount ;</xsl:text><xsl:text>
         foaf:accountServiceHomepage &lt;http://qa.debian.org/developer.php?login=</xsl:text><xsl:value-of select="$escaped-email"/><xsl:text>&gt; ;</xsl:text><xsl:text>
	 owl:sameAs &lt;</xsl:text><xsl:value-of select="$webid-uri"/><xsl:text>#account&gt;
      ] ;</xsl:text><xsl:text>
      owl:sameAs &lt;</xsl:text><xsl:value-of select="$webid-uri"/><xsl:text>#agent&gt;
    ]</xsl:text>
</xsl:template>

<xsl:template name="contributor">
  <xsl:text>
    schema:contributor </xsl:text>

    <xsl:call-template name="online-account">
      <xsl:with-param name="escaped-email">
	<xsl:call-template name="escape-name">
	  <xsl:with-param name="text"><xsl:value-of select="email"/></xsl:with-param>
	</xsl:call-template>
      </xsl:with-param>
      <xsl:with-param name="escaped-name">
	<xsl:call-template name="escape-name">
	  <xsl:with-param name="text"><xsl:value-of select="name"/></xsl:with-param>
	</xsl:call-template>
      </xsl:with-param>
      <xsl:with-param name="webid-uri">
	<xsl:call-template name="email-to-webid-uri">
	  <xsl:with-param name="text"><xsl:value-of select="email"/></xsl:with-param>
	</xsl:call-template>
      </xsl:with-param>
    </xsl:call-template>

    <xsl:text> ;</xsl:text>

</xsl:template>

<xsl:template name="contributors">
  <xsl:for-each select="maintainer">
    <xsl:call-template name="contributor" />
  </xsl:for-each>
  <xsl:for-each select="uploaders/item">
    <xsl:call-template name="contributor" />
  </xsl:for-each>
</xsl:template>


<xsl:template name="link-ubuntu-release">
  <xsl:param name="uri" select="''" />
  <xsl:text>
    doap:release &lt;</xsl:text><xsl:value-of select="$uri" /><xsl:text>&gt; ;</xsl:text>
</xsl:template>

<xsl:template name="related-ubuntu-release">
  <xsl:param name="uri" select="''" />
  <xsl:param name="version" select="''" />  
<xsl:text>
    dcterms:relation &lt;</xsl:text><xsl:value-of select="$uri" /><xsl:text>/</xsl:text><xsl:value-of select="$version" /><xsl:text>&gt; ;</xsl:text>
</xsl:template>


<xsl:template name="ubuntu-release">
  <xsl:param name="project-uri" select="''" />  
  <xsl:param name="version" select="''" />  


<xsl:text>
&lt;</xsl:text><xsl:value-of select="$project-uri" /><xsl:text>/</xsl:text><xsl:value-of select="$version" /><xsl:text>&gt;</xsl:text><xsl:text>
    dcterms:description "</xsl:text><xsl:value-of select="$package" /><xsl:text> </xsl:text><xsl:value-of select="$version" /><xsl:text> source package in Ubuntu" ;</xsl:text><xsl:text>
    rdfs:label "</xsl:text><xsl:value-of select="$package" /><xsl:text> </xsl:text><xsl:value-of select="$version" /><xsl:text>" ;</xsl:text><xsl:text>
    doap:revision "</xsl:text><xsl:value-of select="$version" /><xsl:text>" ;</xsl:text><xsl:text>
    admssw:project &lt;</xsl:text><xsl:value-of select="$project-uri" /><xsl:text>#project&gt; ;</xsl:text><xsl:text>
    dcterms:publisher &lt;http://ubuntu.com/&gt; ;</xsl:text><xsl:text>
    admssw:status &lt;</xsl:text><xsl:value-of select="$released-URI" /><xsl:text>&gt; ;</xsl:text><xsl:text>
    a admssw:SoftwareRelease .

</xsl:text>

</xsl:template>

<xsl:template name="ubuntu-source">
    <xsl:param name="uri" />
    <xsl:param name="version" />

<xsl:text>
&lt;</xsl:text><xsl:value-of select="$uri" /><xsl:text>#project&gt;</xsl:text><xsl:text>
    doap:name "Ubuntu </xsl:text><xsl:value-of select="$package" /><xsl:text> packaging" ;</xsl:text><xsl:text>
    doap:description "Maintenance of the </xsl:text><xsl:value-of select="$package" /><xsl:text> source package in Ubuntu" ;</xsl:text><xsl:text>
    doap:homepage &lt;</xsl:text><xsl:value-of select="$uri" /><xsl:text>&gt; ;</xsl:text><xsl:text>
    admssw:forkOf &lt;</xsl:text><xsl:call-template name="project-URI" /><xsl:text>&gt; ;</xsl:text>

    <xsl:call-template name="link-ubuntu-release">
	<xsl:with-param name="uri" select="concat($uri, '/', $version)" />
    </xsl:call-template>

    <xsl:text>
    a admssw:SoftwareProject .

</xsl:text>

    <xsl:call-template name="ubuntu-release">
      <xsl:with-param name="project-uri" select="$uri" />
      <xsl:with-param name="version" select="$version" />
    </xsl:call-template>


</xsl:template>


<!-- Binds a release/distribution to a SoftwareRelease -->

<xsl:template name="distribution-version">
  <xsl:param name="suite" select="''" />
  <xsl:variable name="version">
      <xsl:value-of select="document(concat('../base/', $dir, '/', $suite,
			    '.xml'))/source/version"/>
  </xsl:variable>
  <xsl:text>
&lt;http://www.debian.org/releases/</xsl:text><xsl:copy-of select="$suite" /><xsl:text>#distribution&gt;</xsl:text><xsl:text>
    admssw:includedAsset &lt;</xsl:text><xsl:call-template name="release-URI"><xsl:with-param name="version" select="$version"/></xsl:call-template><xsl:text>&gt; .
  </xsl:text>
</xsl:template>

<!-- List all release/distribution's releases -->

<xsl:template name="list-release-versions">
      <xsl:if test="$hasoldstable">  
	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">oldstable</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
      <xsl:if test="$hassecurity-oldstable">
	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">security-oldstable</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
      <xsl:if test="$hasoldstable-updates">
      	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">oldstable-updates</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
      <xsl:if test="$hasoldstable-proposed-updates">  
	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">oldstable-proposed-updates</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
      <xsl:if test="$hasoldstable-backports">  
	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">oldstable-backports</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
      <xsl:if test="$hasoldstable-backports-sloppy">  
	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">oldstable-backports-sloppy</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
      <xsl:if test="$hasstable">  
	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">stable</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
      <xsl:if test="$hassecurity-stable">
      	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">security-stable</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
      <xsl:if test="$hasstable-updates">
      	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">stable-updates</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
      <xsl:if test="$hasstable-proposed-updates">  
	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">stable-proposed-updates</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
      <xsl:if test="$hasstable-backports">
      	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">stable-backports</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
      <xsl:if test="$hastesting">
	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">testing</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
      <xsl:if test="$hassecurity-testing">
	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">security-testing</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
      <xsl:if test="$hastesting-proposed-updates">
	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">testing-proposed-updates</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
      <xsl:if test="$hasunstable">
	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">unstable</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
      <xsl:if test="$hasexperimental">
	  <xsl:call-template name="distribution-version">
	    <xsl:with-param name="suite">experimental</xsl:with-param>
	  </xsl:call-template>
      </xsl:if>
</xsl:template>

<!-- <xsl:stylesheet -->
<!--   xmlns:xsl="http://www.w3.org/1999/XSL/Transform" -->
<!--   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" -->
<!--   xmlns:schema="http://schema.org/" -->
<!--   version="1.0"> -->

<xsl:template match="/"><xsl:text>
@prefix rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#&gt; .</xsl:text><xsl:text>
@prefix dcterms: &lt;http://purl.org/dc/terms/&gt; .</xsl:text><xsl:text>
@prefix foaf: &lt;http://xmlns.com/foaf/0.1/&gt; .</xsl:text><xsl:text>
@prefix doap: &lt;http://usefulinc.com/ns/doap#&gt; .</xsl:text><xsl:text>
@prefix schema: &lt;http://schema.org/&gt; .</xsl:text><xsl:text>
@prefix spdx: &lt;http://www.spdx.org/rdf/terms#&gt; .</xsl:text><xsl:text>
@prefix adms: &lt;http://www.w3.org/ns/adms#&gt; .</xsl:text><xsl:text>
@prefix admssw: &lt;http://purl.org/adms/sw/&gt; .</xsl:text><xsl:text>
@prefix owl: &lt;http://www.w3.org/2002/07/owl#&gt; .

</xsl:text>
  <xsl:apply-templates select="//source"/>
</xsl:template>
    

<!-- All the work is done in a single template -->
<xsl:template match="source">

<xsl:text>
&lt;</xsl:text><xsl:call-template name="project-URI" /><xsl:text>&gt;</xsl:text><xsl:text>
    doap:name "Debian </xsl:text><xsl:value-of select="$package" /><xsl:text> packaging" ;</xsl:text><xsl:text>
    doap:description "Maintenance of the </xsl:text><xsl:value-of select="$package" /><xsl:text> source package in Debian" ;</xsl:text><xsl:text>
    doap:homepage &lt;http://packages.debian.org/src:</xsl:text><xsl:value-of select="$package" /><xsl:text>&gt; ;</xsl:text><xsl:text>
    doap:homepage &lt;http://packages.qa.debian.org/</xsl:text><xsl:value-of select="$package" /><xsl:text>&gt; ;</xsl:text>


      <xsl:call-template name="iterate-software-releases">
        <xsl:with-param name="list-of-unique-versions" select="$list-of-unique-versions" />
      	<xsl:with-param name="mode" select="'link'" />
      </xsl:call-template>

      <xsl:call-template name="contributors" />

      <xsl:text>
    a admssw:SoftwareProject .

</xsl:text>

<xsl:call-template name="list-release-versions" />

<xsl:call-template name="iterate-software-releases">
  <xsl:with-param name="list-of-unique-versions" select="$list-of-unique-versions" />
  <xsl:with-param name="mode" select="'full'" />
</xsl:call-template>

<xsl:if test="$other/@ubuntu='yes'">
  <xsl:call-template name="ubuntu-source">
    <xsl:with-param name="uri" select="$other/ubuntu/@url" />
    <xsl:with-param name="version" select="$other/ubuntu/@version" />
  </xsl:call-template>
</xsl:if>



<xsl:text>&lt;http://packages.qa.debian.org/</xsl:text><xsl:value-of select="$dir" /><xsl:text>.ttl&gt;</xsl:text><xsl:text>
    a foaf:Document ;</xsl:text><xsl:text>
    foaf:primaryTopic &lt;</xsl:text><xsl:call-template name="project-URI" /><xsl:text>&gt; ;</xsl:text><xsl:text>
    dcterms:modified "</xsl:text><xsl:value-of select="$date"/><xsl:text>"^^&lt;http://www.w3.org/2001/XMLSchema#dateTime&gt; .

</xsl:text>

</xsl:template>

</xsl:stylesheet>
