#!/usr/bin/python
# -*- coding: utf-8 -*-

# Make sure tabs expand to 8 spaces in vim
# vim: expandtab

import os.path

root = os.path.dirname(os.path.dirname(__file__))
dir = os.path.join(root, "incoming")
odir = os.path.join(root, "base")
webdir = os.path.join(root, "web")
bindir = os.path.join(root, "bin")
libdir = os.path.join(root, "bin")

# use file:// for a local mirror, http:// for a remote mirror
mirrors = ["http://deb.debian.org/debian/" ] # note the trailing slash
