#!/usr/bin/python
# -*- coding: utf-8 -*-

# Make sure tabs expand to 8 spaces in vim
# vim: expandtab

# Copyright 2002-2011 Raphaël Hertzog
# Copyright 2006 Jeroen van Wolffelaar
# Copyright 2011 Jan Dittberner
# This file is distributed under the terms of the General Public License
# version 2 or (at your option) any later version.

import os, rfc822, sys, string, re, email, time, common, subprocess
import xml.dom

from config import dir, odir, root
from common import hash_name

os.environ["PATH"] = "/bin:/sbin:/usr/bin:/usr/sbin"

def read_msg(file):
    f = open(file, "r")
    msg = email.message_from_file(f)
    f.close()
    return msg

def create_html_file(pkg, source, htmlfile):
    # Fixme: old code had some mechanism to clean attachments, apparantly
    # produced by mhonarc
    dir = os.path.dirname(htmlfile)
    if not os.path.exists(dir): os.makedirs(dir)

    mhproc = subprocess.Popen("""cd %s; \
        mhonarc -rcfile /dev/stdin \
        -rcfile %s/etc/mhonarc.rc \
        -single %s 2> /dev/null""" % (dir, root, source), shell=True,
        stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
        close_fds=True)
    (stdin, stdout, stderr) = (mhproc.stdin, mhproc.stdout, mhproc.stderr)
    stdin.write("""
     <MSGHEAD>
     <h3><a href="../../%s.html">Back to %s PTS page</a></h3>
     </MSGHEAD>
    """ % (pkg, pkg))
    stdin.close()
    html = open(htmlfile+".new", "w")
    html.writelines(stdout.readlines())
    for f in html, stdout, stderr: f.close()
    os.chmod(htmlfile+".new", 0664)
    os.rename(htmlfile+".new", htmlfile)

def timestamp_to_rfc822date(timestamp):
    """Format a timestamp in format YYYYMMDDThhmmssZ to a RFC 822 String.
    
    >>> timestamp_to_rfc822date('20090718T163915Z')
    'Sat, 18 Jul 2009 16:39:15 GMT'
    """
    return time.strftime("%a, %d %b %Y %H:%M:%S GMT",
                         time.strptime(timestamp, '%Y%m%dT%H%M%SZ'))

def add_resume_for_msg(pkg, file, elt, doc, kind):
    if not os.path.isfile(file):
        pass

    msg = read_msg(file)
    info = common.extract_info(msg)
    sub_elt = doc.createElement("item")
    sub_elt.appendChild(doc.createTextNode(info["subject"]))
    if info.has_key("url"):
        sub_elt.setAttribute("url", info["url"])
    else:
        htmlfile = pkg+"/"+kind+"/"+info["timestamp"]+".html"
        hash = hash_name(pkg)
        htmlpath = "%s/web/%s/%s" % (root, hash, htmlfile)
        if not os.path.exists(htmlpath):
            create_html_file(pkg, file, htmlpath)
        sub_elt.setAttribute("url", htmlfile)
    if info.has_key("date"):
        sub_elt.setAttribute("date", info["date"])
    sub_elt.setAttribute("rfc822date",
                         timestamp_to_rfc822date(info["timestamp"]))
    if info.has_key("from_name"):
        sub_elt.setAttribute("from", info["from_name"])
    elt.appendChild(sub_elt)


if __name__ == '__main__':
    # Create the XML documents
    while 1:
        line = sys.stdin.readline()
        if not line: break #eof
        pkg = line.strip()

        doc = xml.dom.getDOMImplementation('minidom').createDocument(None, "news", None)
        root_elt = doc.documentElement
        hash = hash_name(pkg)

        # Get news information : static/news/auto
        for type, number in [("static", 5), ("news", 30)]:
            elt = doc.createElement(type)
            dir = "%s/base/%s/%s/%s" % (root, hash, pkg, type)
            if not os.path.exists(dir): os.makedirs(dir)
            entries = os.listdir(dir)
            entries.sort()
            entries.reverse()
            for entry in entries[:number]:
                add_resume_for_msg(pkg, dir+"/"+entry, elt, doc, type)
            root_elt.appendChild(elt)

        # Output the data to the XML file
        try:
            targetfn = "%s/%s/%s/news.xml" % (odir, hash, pkg)
            tempfn = "%s.%d" % (targetfn, os.getpid())
            f = open(tempfn, "w")
            f.write(doc.toxml(encoding='UTF-8'))
            f.close()
            os.rename(tempfn, targetfn)
        except Exception, msg:
            sys.stderr.write("Can't update news.xml for %s\n%s\n" % (pkg, msg))
