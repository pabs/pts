# -*- coding: utf-8 -*-

# Copyright © 2008  Stefano Zacchiroli <zack@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

import os
from lxml import etree

import config
from common import hash_name


__dom = {}
def _dom(pkg):
    try:
        return __dom[pkg]
    except KeyError:
        html = os.path.join(config.webdir, hash_name(pkg), pkg) + ".html"
        if os.path.isfile(html):
            doc = etree.parse(html)
            __dom[pkg] = doc
            return doc

def as_xpath(expr, pkg):
    return _dom(pkg).xpath(expr,
                           namespaces={'xhtml': 'http://www.w3.org/1999/xhtml'})

def singleton(ret):
    if not ret:
        return None
    elif isinstance(ret, list):
        return ret[0]
    else:	# already a singleton value
        return ret

def to_string(ret):
    return unicode(ret).encode('utf-8') # SOAP messages are encoded in utf-8

def to_int(ret):
    if ret is None:
        return 0
    else:
        return int(to_string(ret))

def dict_of_list(l):
    """Takes a flat list [key1, val1, key2, val2, ...] and builds a
    dict mapping keys to values.

    The list is emptied as a (not so nice) side-effect.
    """
    d = {}
    while l:
        k = l.pop()
        v = l.pop()
        d[k] = v
    return d

to_strings = lambda rets: map(to_string, rets)
to_ints = lambda rets: map(to_int, rets)

as_id = lambda id, pkg: \
    singleton(as_xpath("//*[@id='%s']/child::text()" % id, pkg))

