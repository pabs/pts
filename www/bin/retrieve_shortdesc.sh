#!/bin/bash
# Copyright 2008 Stefano Zacchiroli <zack@debian.org>

# This file is distributed under the terms of the General Public License
# version 2 or (at your option) any later version.

# Retrieve most recent package (short) descriptions from UDD
# Dump the results to stdout as rows: BINARY_PKG\tSHORTDESC
# This script is used as a QA cronjob, see /srv/qa.debian.org/data/cronjobs/

#db_host="udd.debian.net"
#db_database="udd"
#db_user="pts"
db_service="udd"

shortdesc_query="
select distinct packages.package, packages.description
from packages,
  (select package, max(version) from packages group by package)
    as latest (package, version)
where packages.package = latest.package
  and packages.version = latest.version
;"

#psql="psql -h $db_host -d $db_database -U $db_user"
psql="psql service=udd"

$psql -t -A -F "	" -c "$shortdesc_query"

