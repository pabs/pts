#!/usr/bin/python
# -*- coding: utf-8 -*-

# Make sure tabs expand to 8 spaces in vim
# vim: expandtab

# Copyright © 2002-2011 Raphaël Hertzog
# Copyright © 2009 Stefano Zacchiroli
# This file is distributed under the terms of the General Public License
# version 2 or (at your option) any later version.

import os.path, rfc822, sys, string, re, cPickle
import xml.dom
from xml.dom.minidom import parseString

from config import dir, odir
from common import hash_name

bug_url_RE = re.compile(r'(https?://bugs\.debian\.org/(\d+))')

def striphtml(input):
    cleared = input.strip()
    # I have to clean it this way first because dependencies are copied raw
    # in update_excuses.html ...
    cleared = cleared.replace(">=", "&gt;=");
    cleared = cleared.replace("<=", "&lt;=");
    cleared = cleared.replace(">>", "&gt;&gt;");
    cleared = cleared.replace("<<", "&lt;&lt;");
    # Remove the tags
    while cleared.find("<") != -1 and cleared.find(">") != -1:
        cleared = cleared[:cleared.find("<")] + cleared[cleared.find(">")+1:]
    # But I really want the output in plain text
    cleared = cleared.replace("&gt;=", ">=");
    cleared = cleared.replace("&lt;=", "<=");
    cleared = cleared.replace("&gt;&gt;", ">>");
    cleared = cleared.replace("&lt;&lt;", "<<");
    return cleared

def bug_link_node(doc, bugno):
    a = doc.createElement("a")
    a.setAttribute("href", "https://bugs.debian.org/" + bugno)
    a.appendChild(doc.createTextNode("#" + bugno))
    return a

# Load the list of excuses generated last time
old_done = {}
new_done = {}
if os.path.exists(odir + "/excuses_done"):
    f = open(odir + "/excuses_done", "r")
    old_done = cPickle.load(f)
    f.close()

f = open(dir + "/update_excuses.html", "r")
# Ignore everything until first list
while string.find(f.readline(), "<ul>") == -1: 
    pass

top = 1
package = ""
hash = ""
doc = None
while 1:
    line = f.readline()
    if not line: break #eof
    if line.find("</ul>") != -1: 
        top = 1
        if "/" in package:
            # Binaries for a specific arch moving, nothing interesting for us
            old_done[package.split("/")[0]] = 1
            continue
        if os.path.exists("%s/%s/%s" % (odir, hash, package)):
            excf = open("%s/%s/%s/excuse.xml" % (odir, hash, package), "w")
            excf.write(doc.toxml(encoding="UTF-8"))
            excf.close()
            new_done[package] = 1
        continue
    if line.find("<ul>") != -1: 
        top = 0
        continue
    # The rest depends of the state
    if top:
        words = re.split("[><() ]", line)
        package = words[6]
        hash = hash_name(package)
        # Create the XML Document
        doc = xml.dom.getDOMImplementation('minidom').createDocument(None, "excuse", None)
        main_elt = doc.documentElement
        top = 0
    else:
        component = 'main'
        for subline in line.split("<li>"):
            if not subline: continue
            subline = subline.strip()
            if subline.find("Section:") != -1:
                component = re.sub(r'Section: *(.*)', '\\1', subline)
            if subline.find("Maintainer:") != -1: continue
            elif subline.find("Too young, only") != -1:
                words = subline.split()
                main_elt.setAttribute("age", words[3])
                main_elt.setAttribute("limit", words[5])
                main_elt.setAttribute("progress", "%d" % (100.0 * string.atoi(words[3]) / string.atoi(words[5])))
                subline = striphtml(subline)
                sub_elt = doc.createElement("item")
                sub_elt.appendChild(doc.createTextNode(subline))
                main_elt.appendChild(sub_elt) 

            elif subline.find("days old (needed") != -1:
                words = subline.split()
                main_elt.setAttribute("age", words[0])
                main_elt.setAttribute("limit", words[4])
                # Being at the last day isn't yet problematic            
                if words[0] != words[4]:
                    main_elt.setAttribute("problematic", "yes")
                subline = striphtml(subline)
                sub_elt = doc.createElement("item")
                sub_elt.appendChild(doc.createTextNode(subline))
                main_elt.appendChild(sub_elt) 

            elif subline.find("Not considered") != -1:
                sub_elt = doc.createElement("item")
                sub_elt.setAttribute("url", "https://qa.debian.org/excuses.php?package=" + package)
                sub_elt.appendChild(doc.createTextNode(subline))
                main_elt.appendChild(sub_elt)

            elif subline.find("introduces new bugs") != -1:
                bugs = []
                m = bug_url_RE.search(subline)
                while m:
                    _url, bug_no = m.groups()
                    bugs.append(bug_no)
                    m = bug_url_RE.search(subline, m.end())

                # Compute the item's data only if there are bugs found
                if bugs:
                    sub_elt = doc.createElement("item")
                    sub_elt.setAttribute("type", "raw")
                    sub_elt.appendChild(doc.createTextNode(
                            # We cannot use "package" here as it can
                            # be the binary package name, and not the
                            # source package.
                            subline[0 : subline.find(":")+2]
                            ))
                    main_elt.appendChild(sub_elt)
                    first = True
                    for bug in bugs:
                        if first:
                            first = False
                        else:
                            sub_elt.appendChild(doc.createTextNode(', '))
                        sub_elt.appendChild(bug_link_node(doc, bug))
                    sub_elt.appendChild(doc.createTextNode('.'))

            else:
                sub_elt = doc.createElement("item")
                sub_elt.setAttribute("type", "raw")

                # Strip leading <li>, if any
                line = subline
                start = line.find("<li>")
                if start != -1:
                    line = line[start+4:]

                # Convert some special characters to HTML entities
                # (otherwise, the parser fails)
                line = line.replace('&', '&amp;') \
                    .replace(">=", "&gt;=") \
                    .replace("<=", "&lt;=") \
                    .replace(">>", "&gt;&gt;") \
                    .replace("<<", "&lt;&lt;")

                # Parse the line and fix links
                try:
                    children = parseString("<div>%s</div>" % line).firstChild.childNodes
                    for my_node in tuple(children):
                        if my_node.nodeType == xml.dom.Node.ELEMENT_NODE and my_node.hasAttribute('href'):
                            new_href = re.sub(r'#(.*)',
                                              'https://packages.qa.debian.org/\\1',
                                              my_node.getAttribute('href'))
                            my_node.setAttribute('href', new_href)
                        sub_elt.appendChild(my_node)
                    main_elt.appendChild(sub_elt)
                except xml.parsers.expat.ExpatError:
                    main_elt.appendChild(doc.createTextNode(striphtml(subline)))

f.close()

# Store the new list
f = open(odir + "/excuses_done", "w")
cPickle.dump(new_done, f, 0)
f.close()

# Find excuses generated last time and not this time
# and remove them
for p in old_done.keys():
    if not new_done.has_key(p):
        hash = hash_name(p)
        filename = "%s/%s/%s/excuse.xml" % (odir, hash, p)
        filenamerebuild = "%s/%s/%s/force-rebuild" % (odir, hash, p)
        if os.path.exists(filename):
            os.unlink(filename)
            f = open(filenamerebuild, "w")
            f.close()

# We're done
