#!/bin/bash

# Copyright 2002-2009 Raphaël Hertzog
# Copyright 2007-2008 Stefano Zacchiroli
# This file is distributed under the terms of the General Public License
# version 2 or (at your option) any later version.

# This script will process a list of source packages in input, and
# invoke the XSL stylesheets to generate the static HTML and RDF
# documents available on the Web interface of the PTS

verbose=
debug=

PATH="/bin:/sbin:/usr/bin:/usr/sbin"

if [ -d "incoming" ]; then
    root="$PWD"
elif [ -d "../incoming" ]; then
    root="$PWD/.."
else
    root="/srv/packages.qa.debian.org/www"
fi

xsldir=$root/xsl
#rdf_stylesheet=$xsldir/admssw.xsl
rdf_stylesheet=$xsldir/admssw-turtle.xsl


if [ "$1" = "-v" -o "$2" = "-v" ]; then
    verbose=1
fi
if [ "$1" = "-d" -o "$2" = "-d" ]; then
    verbose=1
    debug=1
fi
if [ "$1" = "-f" -o "$2" = "-f" ]; then
    force="yes"
fi

if [ -n "$debug" ]; then
    echo "cd $root/base"
fi
cd $root/base
date=`LC_ALL=C date -u "+%FT%T%:z"`
gitrev=$(git --git-dir $root/../.git describe --always)

# Read source package names on stdin
while read package
do
    hash=${package:0:1}
    if [ "${package:0:3}" = "lib" ]; then
	hash=${package:0:4}
    fi
    dir=$hash/$package
    htmlfile=$root/web/$dir.html
    rssfile=$root/web/$dir/news.rss20.xml
    rdffile=$root/web/$dir.rdf
    turtlefile=$root/web/$dir.ttl
    needhtmlupdate="no"
    needrdfupdate="no"
    param=""
    param="$param --stringparam gitrev $gitrev"
    param="$param --stringparam dir $dir"
    param="$param --stringparam package $package"
    param="$param --stringparam hash $hash"
    # If the file doesn't exist, then require a generation !
    if [ ! -f $htmlfile ]; then
	needhtmlupdate="yes"
    fi
    if [ ! -f $turtlefile ]; then
	needrdfupdate="yes"
    fi
    # Force regeneration
    if [ "$force" = "yes" ]; then
	needhtmlupdate="yes"
	needrdfupdate="yes"
    fi
    # Check if force-rebuild exists
    if [ -e $dir/force-rebuild ]; then
	needhtmlupdate="yes"
	needrdfupdate="yes"
    fi
    # If the XSL file is newer than the HTML file, then force update
    for i in "$xsldir/pts.xsl" "$xsldir/pts-issues.xsl" "$xsldir/common-params-vars.xsl"
    do
	if [ "$i" -nt "$htmlfile" ]; then
	    needhtmlupdate="yes"
	fi
    done
    # If the XSL file is newer than the RDF file, then force update
    for i in "$rdf_stylesheet" "$xsldir/common-params-vars.xsl"
    do
	if [ "$i" -nt "$turtlefile" ]; then
	    needrdfupdate="yes"
	fi
    done
    # Create parameters and check if an update is needed
    # XXX order _does_ matter, the latter has precedence over the former
    for i in news excuse other \
      mentors \
      oldoldstable-proposed-updates oldstable-proposed-updates \
      stable-proposed-updates testing-proposed-updates \
      oldoldstable-updates oldstable-updates stable-updates \
      oldoldstable-backports oldoldstable-backports-sloppy \
      oldstable-backports oldstable-backports-sloppy \
      stable-backports \
      security-oldoldstable security-oldstable security-stable security-testing \
      oldoldstable oldstable stable experimental testing unstable
    do
	if [ -f "$dir/$i.xml" ]; then
	    # Input is the main xml file used by xsltproc to generate the HTML
	    # page. By default it is unstable.xml but it has many
	    # fallbacks (testing.xml, stable.xml, experimental.xml, ...)
	    if [ "$i" != "mentors" ] ; then
	      # mentors.d.n needs special handling, because we don't want a PTS
	      # page to exist for packages only available on mentors.d.n
	      input=$dir/$i.xml
	    fi
	    param="$param --stringparam has$i yes"
	fi
	if [ "$needhtmlupdate" = "no" -a "$dir/$i.xml" -nt "$htmlfile" ]; then
	    needhtmlupdate="yes"
	fi
	if [ "$needrdfupdate" = "no" -a "$dir/$i.xml" -nt "$turtlefile" ]; then
	    needrdfupdate="yes"
	fi
    done
    # If no distribution-related input files has been found, 
    # then the package has been removed
    if [ "$input" = "$dir/other.xml" ]; then
	input="$xsldir/default.xml"
	param="$param --stringparam removed yes"
	if [ "$needhtmlupdate" = "no" -a "$xsldir/default.xml" -nt "$htmlfile" ]; then
	    needhtmlupdate="yes"
	fi
	if [ "$needrdfupdate" = "no" -a "$xsldir/default.xml" -nt "$turtlefile" ]; then
	    needrdfupdate="yes"
	fi
    else
	param="$param --stringparam removed no"
    fi
    if [ ! -d ../web/$dir/static ]; then
	mkdir -p ../web/$dir/{static,news} || true
    fi
    errors="no"
    if [ "$needhtmlupdate" = "yes" ]; then
	if [ -n "$verbose" ]; then
	    echo "Doing $package ..."
	fi
	xsltproc -o $htmlfile \
	  --stringparam date "$date" $param \
	  $xsldir/pts.xsl $input
        perl -i -p0e 's{>\s*</dt>}{></dt>}smg' $htmlfile
        test "$?" -ne 0 && errors="yes"
	if [ -f "$dir/news.xml" ]; then
	  xsltproc -o $rssfile \
	    --stringparam date "$date" $param \
	    $xsldir/news2rss.xsl $dir/news.xml
          test "$?" -ne 0 && errors="yes"
	fi
    else
	if [ -n "$verbose" ]; then
	    echo "Not doing html for $package : update not needed (use -f to force) ..."
	fi
    fi
    if [ "$needrdfupdate" = "yes" ]; then
	if [ -n "$debug" ]; then
	    echo "xsltproc -o $turtlefile --stringparam date '$date' $param $rdf_stylesheet $input"
	fi
	xsltproc -o $turtlefile \
	  --stringparam date "$date" $param \
	  $rdf_stylesheet $input
        if [ $? -ne 0 ]; then
	    errors="yes"
	else
	    # generate the RDF/XML file from the Turtle one
	    rapper -q -i turtle -o rdfxml-abbrev $turtlefile > $rdffile
	    test "$?" -ne 0 && errors="yes"
	fi
    else
	if [ -n "$verbose" ]; then
	    echo "Not doing rdf $package : update not needed (use -f to force) ..."
	fi
    fi
    test "$errors" = "yes" && \
        echo "PTS: non-0 exit code while generating output for package $package"
    rm -f $dir/force-rebuild || true
done

# Then update the software repository descriptor
cd $root
$root/bin/generate-admssw-repository.sh
