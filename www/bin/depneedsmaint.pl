#!/usr/bin/perl -w

# Copyright (C) 2012 Bart Martens <bartm@knars.be>
# Copyright (C) 2015 Paul Wise <pabs@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strict;
use warnings;
use SOAP::Lite;
use Dpkg::Deps qw(deps_parse deps_iterate);
use Dpkg::ErrorHandling qw(report_options);
use Dpkg::Control::Fields qw(field_list_src_dep field_list_pkg_dep field_get_dep_type);

my %needsmaint;

# Silence dpkg warnings about deprecated relations
report_options(quiet_warnings => 1);

# Not interested in some of the fields
my %good_pkg = map { $_ => 1 } qw(Enhances Built-Using);
my %src_deps = map { $_ => 1 } grep { field_get_dep_type($_) ne 'union' } field_list_src_dep();
my %pkg_deps = map { $_ => 1 } grep { $good_pkg{$_} || field_get_dep_type($_) ne 'union' } field_list_pkg_dep();

# Work around SOAP::Lite not being able to verify certs correctly
my $ca_dir = '/etc/ssl/ca-debian';
$ENV{PERL_LWP_SSL_CA_PATH} = $ca_dir if -d $ca_dir;

my $soap = SOAP::Lite->uri('Debbugs/SOAP')->proxy('https://bugs.debian.org/cgi-bin/soap.cgi');

my @buglist;
{
	my $bugs = $soap->get_bugs(package=>'wnpp',status=>'open')->result();
	push @buglist, @$bugs;
	$bugs = $soap->get_bugs(package=>'wnpp',status=>'forwarded')->result();
	push @buglist, @$bugs;
}

my $wnppbuginfo = {};
while (my @slice = splice(@buglist, 0, 500))
{
	my $tmp = $soap->get_status(@slice)->result;
	next unless $tmp;
	%$wnppbuginfo = (%$wnppbuginfo, %$tmp);
}

foreach my $bug ( keys %$wnppbuginfo )
{
	my $subject = $wnppbuginfo->{$bug}->{'subject'};

	$subject =~ s/^(RFP:)(\S)/$1 $2/;
	$subject =~ s/^'(ITP: )/$1/;
	$subject =~ s/^([A-Z]+: \S+)  ?--? .*$/$1/;

	next if( $subject !~ /^([A-Z]+): (\S+)$/ );

	my $type = $1;
	my $package = $2;

	next if( $type !~ /^(O|RFA)$/ );
	next if( defined $needsmaint{$package} and $needsmaint{$package} =~ /^O / );

	$needsmaint{$package} = "$type $bug";
}

sub read_block
{
	my $block = "";

	while( <INPUT> )
	{
		$block .= $_;
		chomp;
		last if( /^$/ );
	}

	$block =~ s%\n(\s)%$1%gs;

	return undef if( $block eq "" );
	return $block;
}

sub simplify_packages_list
{
	my $type = shift;
	my $list = shift;
	if ($src_deps{$type} || $pkg_deps{$type}) {
		my $deps = deps_parse($list, build_dep => $src_deps{$type}, use_arch => 0, host_arch => 'amd64');
		my @deps; deps_iterate($deps, sub { push @deps, shift->{package}; });
		$list = join ',', @deps;
	} elsif ($type eq 'Source') {
		$list =~ s/ .*//;
	} elsif ($type eq 'Binary') {
		$list =~ tr/ //d;
	} else {
		warn "Unknown field: $type: $list";
	}
	$list = ",$list,";

	return $list;
}

# choices to make:
#my @mirrors = ( "debian", "debian-backports", "debian-security", "debian-volatile" );
my @mirrors = ( "debian", "debian-security" );
#my @dists = ( "oldstable", "stable", "testing", "unstable", "experimental" );
my @dists = ( "testing", "unstable", "experimental" );
#my @distvariants = ( "", "-proposed-updates", "-updates", "-backports" );
my @distvariants = ( "", "-proposed-updates", "-updates" );
my @sections = ( "main", "contrib", "non-free" );
my @sourcesfiles;
my @packagesfiles;

foreach my $mirror ( @mirrors )
{
	foreach my $dist ( @dists )
	{
		foreach my $distvariant ( @distvariants )
		{
			foreach my $section ( @sections )
			{
				push @sourcesfiles, "/srv/mirrors/$mirror/dists/$dist$distvariant/$section/source/Sources.gz";
				push @packagesfiles, "/srv/mirrors/$mirror/dists/$dist$distvariant/$section/binary-amd64/Packages.gz";
			}
		}
	}
}

my %bin2src;
my %src2bindep;
my %src2srcdep;

foreach my $sourcesfile ( @sourcesfiles )
{
	next if( ! -e $sourcesfile );
	open INPUT, "zcat $sourcesfile |" or next;
	while( $_ = read_block() )
	{
		my $source = undef;
		my $binary = undef;
		my %deps = ();

		$source = $1 if( /^Package: (.*)$/m );
		$binary = $1 if( /^Binary: (.*)$/m );
		foreach my $type (keys %src_deps) {
			$deps{$1} = $2 if( /^($type): (.*)/m );
		}
	
		die if( not defined $source );
		die if( not defined $binary );

		$binary = simplify_packages_list( 'Binary', $binary );
		$bin2src{$1}{$source} = 1 while( $binary =~ s%,([^,]+),%,% );

		foreach my $deptype ( keys %deps )
		{
			my $deps = simplify_packages_list( $deptype, $deps{$deptype} );
			$src2bindep{$source}{$deptype}{$1} = 1 while( $deps =~ s%,([^,]+),%,% );
		}
	}
	close INPUT;
}

foreach my $packagesfile ( @packagesfiles )
{
	next if( ! -e $packagesfile );
	open INPUT, "zcat $packagesfile |" or next;
	while( $_ = read_block() )
	{
		my $binary = undef;
		my $source = undef;
		my %deps = ();

		$binary = $1 if( /^Package: (.*)/m );
		$source = $1 if( /^Source: (.*)/m );
		foreach my $type (keys %pkg_deps) {
			$deps{$1} = $2 if( /^($type): (.*)/m );
		}
	
		die if( not defined $binary );
		$source = $binary if( not defined $source );

		$source = simplify_packages_list( 'Source', $source );
		$source =~ s%^,([^,]+),$%$1%;

		$bin2src{$binary}{$source} = 1 if( not defined $bin2src{$binary}{$source} );

		foreach my $deptype ( keys %deps )
		{
			my $deps = simplify_packages_list( $deptype, $deps{$deptype} );
			$src2bindep{$source}{$deptype}{$1} = 1 while( $deps =~ s%,([^,]+),%,% );
		}
	}
	close INPUT;
}

foreach my $source ( keys %src2bindep )
{
	next if( defined $needsmaint{$source} );

	foreach my $deptype ( sort keys %{$src2bindep{$source}} )
	{
		foreach my $binary ( keys %{$src2bindep{$source}{$deptype}} )
		{
			next if( not defined $bin2src{$binary} );

			foreach my $source2 ( keys %{$bin2src{$binary}} )
			{
				next if( $source eq $source2 );
				next if( defined $src2srcdep{$source}{$source2} );

				$src2srcdep{$source}{$source2} = "$deptype $binary";
			}
		}
	}

	foreach my $source2 ( sort keys %{$src2srcdep{$source}} )
	{
		next if( not defined $needsmaint{$source2} );

		print "$source $source2 $src2srcdep{$source}{$source2} $needsmaint{$source2}\n";
	}
}

