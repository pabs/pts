#!/usr/bin/python
# vim: expandtab

# Copyright 2009 Raphael Hertzog
# Available under the terms of the General Public License version 2
# or (at your option) any later version.

import cgi, Cookie, os
#import cgitb
#cgitb.enable()

form = cgi.FieldStorage()
if (form.has_key("csspref")):
    c = Cookie.SimpleCookie()
    c["csspref"] = form["csspref"].value
    c["csspref"]["max-age"] = 1531263600
    c["csspref"]["path"] = "/"
    print c

print "Location: " + os.getenv("HTTP_REFERER", "https://packages.qa.debian.org")
print

