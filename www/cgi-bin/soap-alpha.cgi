#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2008 Stefano Zacchiroli <zack@debian.org>
#
# This file is distributed under the terms of the General Public License
# version 3 or (at your option) any later version.

"""
Debian's PTS SOAP API reference
===============================

This is the API reference for U{Debian<https://www.debian.org>}'s
U{Package Tracking System<https://packages.qa.debian.org>} (PTS) U{SOAP
interface<https://wiki.debian.org/qa.debian.org/pts/SoapInterface>}.
For more information, see the U{wiki
page<https://wiki.debian.org/qa.debian.org/pts/SoapInterface>}.

Naming conventions
------------------

- all methods ending with C{_url} return atomic C{str} values denoting
  URLs

"""

import sys

import config
sys.path.insert(0, config.libdir)
from soap_lib import *


__base_version = '0.1'
__version = __base_version


def version():
    """
    Return the SOAP API version

    @rtype: C{str}
    @return: current API version as a string
    """
    global __base_version, __version
    return __version

def latest_version(source):
    """
    Return the latest (known) version of a given source package.

    @type source: C{str}
    @param source: source package name
    @rtype: C{str}
    @return: latest source package version
    """
    return to_string(as_id('latest_version', source))

def versions(source):
    """
    Return all the (known) versions of a given source package.

    @type source: C{str}
    @param source: source package name
    @rtype: C{dict} with C{str} keys and values
    @return: dictionary mapping suite names to the latest known
      version of the package in those suites.

      E.g.: C{{'stable': '1.2-4', 'testing': '1.5-6', 'unstable': '1.5-7'}}
    """
    nodes = as_xpath("//xhtml:span[starts-with(@class,'srcversion')]/@title "
                     "| //xhtml:span[starts-with(@class,'srcversion')]/child::text()",
                     source)
    nodes.reverse()
    return dict_of_list(map(to_string, nodes))

def maintainer_name(source):
    """
    Return the name of the maintainer of a given source package.

    @type source: C{str}
    @param source: source package name
    @rtype: C{str}
    @return: maintainer name
    """
    nodes = as_xpath("//*[starts-with(@class,'maintainer')]"
                     "//*[starts-with(@title,'maintainer')]/child::text()", source)
    return to_string(singleton(nodes))

def maintainer_email(source):
    """
    Return the email of the maintainer of a given source package.

    @type source: C{str}
    @param source: source package name
    @rtype: C{str}
    @return: maintainer email
    """
    nodes = as_xpath("//*[starts-with(@class,'maintainer')]/xhtml:a/@href",
                     source)
    s = to_string(singleton(nodes))
    return s.split('login=')[1]

def maintainer(source):
    """
    Return information about the maintainer of a given source package.

    Maintainer information are currently name and email, as returned
    respectively by L{maintainer_name} and L{maintainer_email}.

    @type source: C{str}
    @param source: source package name
    @rtype: C{dict} with C{str} keys and values
    @return: a dictionary mapping C{'name'} to the maintainer name and
      C{'email'} to the maintainer email
    """
    return {'name': maintainer_name(source), 'email': maintainer_email(source)}

def uploader_names(source):
    """
    Return the names of all the uploaders of a given source package.

    @type source: C{str}
    @param source: source package name
    @rtype: C{list} of C{str}
    @return: uploader name list
    """
    nodes = as_xpath("//*[starts-with(@class,'maintainer')]"
                     "//*[starts-with(@class,'uploader')]"
                     "//*[starts-with(@class,'name')]/child::text()", source)
    return to_strings(nodes)

def uploader_emails(source):
    """
    Return the emails of all the uploaders of a given source package.

    @type source: C{str}
    @param source: source package name
    @rtype: C{list} of C{str}
    @return: uploader email list
    """
    nodes = as_xpath("//*[starts-with(@class,'maintainer')]"
                     "//*[starts-with(@class,'uploader')]"
                     "//xhtml:a/@href", source)
    return map(lambda s: s.split('login=')[1], to_strings(nodes))

def uploaders(source):
    """
    Return information about all the uploaders of a given source package.

    @type source: C{str}
    @param source: source package name
    @rtype: C{list} of C{dict} with C{str} keys and values
    @return: a list of uploader information; information are returned
      in the same format of L{maintainer}
    """
    def combine(names, emails):
        assert len(names) == len(emails)
        return [ { 'name': names[i], 'email': emails[i] }
                 for i in range(len(names)) ]
    return combine(uploader_names(source), uploader_emails(source))

def standards_version(source):
    """
    Return latest C{Standards-Version} value of a given source
    package, i.e., latest version of the U{policy
    manual<https://www.debian.org/doc/debian-policy/>} that the package
    declares to be conforming to.

    @type source: C{str}
    @param source: source package name
    @rtype: C{str}
    @return: latest package standards version
    """
    return to_string(as_id('standards_version', source))

def priority(source):
    """
    Return the C{Priority} value of a given source package.

    @type source: C{str}
    @param source: source package name
    @rtype: C{str}
    @return: package priority
    """
    nodes = as_xpath("//*[@id='priority']/xhtml:small/child::text()", source)
    return to_string(singleton(nodes))

def section(source):
    """
    Return the C{Section} value of a given source package.

    @type source: C{str}
    @param source: source package name
    @rtype: C{str}
    @return: package section
    """
    nodes = as_xpath("//*[@id='section']/xhtml:small/child::text()", source)
    return to_string(singleton(nodes))

def binary_names(source):
    """
    Return the names of all the binary packages of a given source package.

    @type source: C{str}
    @param source: source package name
    @rtype: C{list} of C{str}
    @return: binary package names
    """
    nodes = as_xpath("//xhtml:span[starts-with(@class,'binpkg')]//child::text()",
                     source)
    return to_strings(nodes)

def lintian(source):
    """
    Return the lintian summary for a given source package.

    Currently only warnings and errors are reported.

    @type source: C{str}
    @param source: source package name
    @rtype: C{dict} with C{str} keys and C{int} values
    @return: a dictionary mapping C{'warnings'} to the number of
      lintian warnings and C{'errors'} to that of lintian errors
    """
    warn = as_id('lintian_warnings', source)
    err = as_id('lintian_errors', source)
    return {'warnings': to_int(warn), 'errors': to_int(err)}

def bug_counts(source):
    """
    Return the bug summary for a given source package.

    Merged bugs are I{not} counted more than once in the returned bug
    counts.

    It is not granted that all counts are there in all summaries, some
    are optionals and are marked as such below.

    @type source: C{str}
    @param source: source package name
    @rtype: C{dict} with C{str} keys and C{int} values
    @return: a dictionary with the following mappings:
      - C{'all'}: total number of bugs
      - C{'rc'}: release critical bugs
      - C{'in'}: severity important and normal bugs
      - C{'mw'}: severity medium and wishlist bugs
      - C{'fp'}: fixed and pending bugs
      - C{'help'}: bugs tagged C{help} I{(optional)}
      - C{'newcomer'}: bugs tagged C{newcomer} I{(optional)}
    """
    nodes = as_xpath("//xhtml:span[starts-with(@class,'bugcount')]/@title "
                     "| //xhtml:span[starts-with(@class,'bugcount')]/child::text()",
                     source)
    nodes.reverse()
    return dict_of_list(map(to_string, nodes))

def hyperlinks(source):
    """
    B{NOT IMPLEMENTED}.

    Return a list of hyperlinks related to a given source package.
    """
    raise NotImplementedError('hyperlinks')

def files(source):
    """
    B{NOT IMPLEMENTED}.

    Return (URLs of) files composing a given source package.
    """
    raise NotImplementedError('files')

def ubuntu_info(source):
    """
    B{NOT IMPLEMENTED}.

    Return the summary of Ubuntu-originated information about a given
    source package.
    """
    raise NotImplementedError('ubuntu_info')

def subscriber_count(source):
    """
    B{NOT IMPLEMENTED}.

    Return the number of subscribers to PTS notification for a given
    source package.
    """
    raise NotImplementedError('subscriber_count')

def todos(source):
    """
    B{NOT IMPLEMENTED}.

    Return a list of TODO items related to a given source package.
    """
    raise NotImplementedError('todos')

def problems(source):
    """
    B{NOT IMPLEMENTED}.

    Return a list of problems affecting a given source package.
    """
    raise NotImplementedError('problems')

def news(source):
    """
    B{NOT IMPLEMENTED}.

    Return a list of recent news about a given source package.
    """
    raise NotImplementedError('news')

def news_feed_url(source):
    """
    B{NOT IMPLEMENTED}.

    Return the URL of a RSS feed containing news about a given source
    package.
    """
    raise NotImplementedError('news_feed_url')


from ZSI import dispatch
dispatch.AsCGI(rpc=True)
# dispatch.AsServer(port=9999, rpc=True)

