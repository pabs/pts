#!/usr/bin/python
# vim: expandtab

# Copyright 2002-2003 Raphael Hertzog
# Available under the terms of the General Public License version 2
# or (at your option) any later version.

import cgi, os, sys, re, string
#import cgitb; cgitb.enable()

src = {}
if os.path.isfile("../base/sources.map"):
    f = open("../base/sources.map")
    while 1:
        line = f.readline();
        if not line: break #eof
        line = string.strip(line)
        (binary, source) = string.split(line, None, 1)
        src[binary] = source
    f.close()

error = "404 File not found"
url = ""
if os.environ.has_key("REDIRECT_ERROR_NOTES"):
    error = os.environ["REDIRECT_ERROR_NOTES"]
if os.environ.has_key("REDIRECT_URL"):
    url = os.environ["REDIRECT_URL"]
pkgbin = ""
pkgsrc = ""
hash = ""
srcfile = ""

res = re.match(r"^/.*/(.*)\.html", url)
if res:
    pkgbin = res.group(1)
    if src.has_key(pkgbin):
        pkgsrc = src[pkgbin]
        hash = pkgsrc[0]
        if pkgsrc[0:3] == "lib":
            hash = pkgsrc[0:4]
        srcfile = "../web/%s/%s.html" % (hash, pkgsrc)

if pkgsrc and os.path.isfile(srcfile):
    print "Status: 307 Temporary Redirect"
    print "Location: /%s" % pkgsrc

print "Content-Type: text/html"
print 
print "<html><head><title>Error 404</title>"
if pkgsrc and os.path.isfile(srcfile):
    print "<meta http-equiv='refresh' content='10;url=/%s'>" % pkgsrc
print "</head><body>"

if pkgsrc and os.path.isfile(srcfile):
    print "<h1>Little error ...</h1>"
    print "<p>The page you asked doesn't exist."
    print "However it looks like you were trying to look for `%s' binary package." % pkgbin
    print "The Package Tracking System is source based, therefore you should"
    print "have looked for `%s'." % pkgsrc
    print "<p><a href='/%s'>Click here to be redirected or wait 10 seconds.</a>" % pkgsrc
else:
    print "<h1>Error 404</h1>"
    print "I'm sorry, the page that you asked doesn't exist :<br>"
    print "<pre>%s</pre>" % error
    print "Maybe you've made a mistake and the package you're looking for \
           does not (or no longer) exist"
    
print "</body></html>"

