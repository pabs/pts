#!/bin/sh

dir="/srv/packages.qa.debian.org"

nb_src=`$dir/bin/dump.pl | wc -l | tr -d " "`
nb_sub=`$dir/bin/dump.pl | tr -c -d @ | wc -c | tr -d " "`
nb_email=`$dir/bin/dump.pl | tr " " "\n" | grep @ | sort -u | wc -l | tr -d " "`

printf "Src pkg\tSubscr.\tDate\t\tNb email\n"
printf "$nb_src\t$nb_sub\t`date +%Y-%m-%d`\t$nb_email\n"
#echo "Average number of subscriptions per package: " $[ $nb_sub / $nb_src ]

