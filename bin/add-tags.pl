#!/usr/bin/perl -w

# Copyright 2006 Raphaël Hertzog <hertzog@debian.org>
# Available under the terms of the General Public License version 2
# or (at your option) any later version

use lib '/srv/packages.qa.debian.org/perl';

use DB_File;

use strict;
use vars qw(%db_tags_content);

require "common.pl";

# Comment those two lines for the few times where this script may be needed
print "You shouldn't call this script.\n";
exit 1;

# Add buildd keyword to people with default keyword
open_db_write();
my $i = 0;
foreach (sort keys %db_tags_content) {
	if ($db_tags_content{$_} !~ m/buildd/) {
		if ($db_tags_content{$_} =~ m/default/) {
			$db_tags_content{$_} =~ s/default/default,buildd/;
			$i++;
		}
	}
}
print "$i entries updated.\n";
close_db();
