#!/usr/bin/perl -w

# Copyright 2006 Raphaël Hertzog <hertzog@debian.org>
# Available under the terms of the General Public License version 2
# or (at your option) any later version

use lib '/srv/packages.qa.debian.org/perl';

use DB_File;

use strict;
use vars qw(%db_bounces_content);

require "common.pl";

# Dump the tag database
open_db_bounces();
foreach (sort keys %db_bounces_content) {
    print $_ . ":" . $db_bounces_content{$_} . "\n";
}
close_db_bounces();
